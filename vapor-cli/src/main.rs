use std::process::ExitCode;
use std::path::PathBuf;

use clap::Parser;
use vapor::error::VaporError;
use vapor::games::Game;

mod help_strings;

/// Used to auto-generate the help text and argument parser
#[derive(Parser)]
#[clap(
	version, name = "vapor",
	arg_required_else_help = true,
	about = "Steam game launcher",
	long_about = help_strings::LONG_ABOUT
)]
struct Args {

	/// ID of the game to run
	#[clap(
		long_help = "ID number of the game to run.\nCannot be used with any other options",
		exclusive = true
	)]
	appid: Option<u32>,

	/// Create or read cache of games library
	#[clap(
		short, long,
		value_name = "PATH",
		long_help = help_strings::CACHE_LONG_HELP
	)]
	cache: Option<Option<PathBuf>>,

	/// Case-insensitive title search. Can't be used without -s or -l
	#[clap(short = 'i', long, requires = "search", requires = "launch")]
	case_insensitive: bool,

	/// Search for and list games matching the given title regex
	#[clap(
		short, long,
		value_name = "NAME",
		conflicts_with = "launch",
		long_help =
			"Search for and list all games with titles matching the given pseudo-PCRE. Games will be listed \
			 alphabetically, along with their ID.\n\
			 Cannot be used with -l"
	)]
	search: Option<String>,

	/// Launch games matching the given title regex
	#[clap(
		short, long,
		short_alias = 'n',
		value_name = "NAME",
		help = "Launch the game matching the given title regex. -n also works",
		long_help =
			"Search for and launch a game with title matching the given pseudo-PCRE. The entire search (-s) procedure \
			 is run, and whatever game would be printed first (i.e. the first alphabetic match) will be launched.\n\
			 -n is a valid alias for this.\n\
			 Cannot be used with -s"
	)]
	launch: Option<String>
}

/// Print the games list. Used in search mode
#[inline(always)]
fn print_games(list: Vec<Game>) {
	for game in list {
		let game: (u32, String) = game.into();
		println!("{:<10} {}", game.0, game.1);
	}
}

/// Print an error and "exit".
///
/// Doesn't actually exit; call this with `return`
fn error_exit(e: VaporError, exit_code: u8) -> ExitCode {
	eprintln!("{e}");
	exit_code.into()
}

// Exit codes:
//
// 0 - success
// 1 - IO error
// 2 - invalid CLI arguments (automatic from clap; not presented here)
// 3 - HOME environment variable couldn't be read
// 4 - game not found
// 5 - regex errors
// 6 - serde cache error
// 7 - serde VDF/ACF error
// 8 - globbed path not Unicode or lacks read permissions
const SUCCESS: u8 = 0;
const IO_ERROR: u8 = 1;
const HOME_ENVVAR: u8 = 3;
const NO_GAME_FOUND: u8 = 4;
const MALFORMED_REGEX: u8 = 5;
const SERDE_ERROR: u8 = 6;
const VDF_ERROR: u8 = 7;
const GLOB_ERROR: u8 = 8;

fn main() -> ExitCode {
	let args = Args::parse();

	//If an appid was specified, just launch it and exit
	if let Some(appid) = args.appid {
		match vapor::launch_appid(appid) {
			Ok(_) => return SUCCESS.into(),
			Err(e) => return error_exit(e, IO_ERROR)
		}
	}

	//We'll need a default cache later
	let default_cache = match vapor::dirs::get_cache() {
		Ok(x) => x,
		Err(e) => return error_exit(e, HOME_ENVVAR)
	};

	//The Option<Option> is only used for clap. We can flatten it safely
	let cache = args.cache.flatten().unwrap_or(default_cache);

	//If this variable is None, we're in cache-only mode. Else, we have to do something with regex
	let (launch, name) = if args.search.is_some() {
		(false, args.search)
	} else {
		(args.launch.is_some(), args.launch)
	};

	//If we're in cache-only mode, just make a cache
	if name.is_none() {
		match vapor::cache::build_cache(&cache) {
			Ok(_) => return SUCCESS.into(),
			Err(e) => return error_exit(e, IO_ERROR)
		}
	}

	//If we're not in cache-only mode, get the NAME regex
	let regex = name.unwrap();

	//If we get this far, we need to search for games

	let vec = match vapor::games::find_games(&regex, args.case_insensitive, &cache) {
		Ok(v) => v,
		Err(e) => match &e {
			VaporError::InvalidRegex(_) => return error_exit(e, MALFORMED_REGEX),
			VaporError::IoError(_) => return error_exit(e, IO_ERROR),
			VaporError::SerdeFailure(_) => return error_exit(e, SERDE_ERROR),
			VaporError::VdfParseFailure(_) => return error_exit(e, VDF_ERROR),
			VaporError::GlobFailure => return error_exit(e, GLOB_ERROR),
			_ => unreachable!() //TODO: double-check, is this *really* the complete list of possibilities?
		}
	};

    //Finally: either list the search results, launch a game, or return an error
	if launch {
		if vec.is_empty() {
			return NO_GAME_FOUND.into();
		}

		if let Err(e) = vapor::launch_game(&vec[0]) {
			return error_exit(e, IO_ERROR);
		}
	} else {
		print_games(vec);
	}

	0.into()
}
