#[cfg(target_family = "windows")]
pub static LONG_ABOUT: &str =
	"Launches Steam games by name. Assumes the default steamapps directory, but a config file can override this; it \
	need only contain a path to a steamapps directory. The config file is `%APPDATA%\\Vapor\\vapor.conf`. If a cache \
	exists, search will use the cache instead of the steamapps directory; see -c\n\
	\n\
	Exit code 0 signals successful operation. Codes 1 through 8 signal the following errors:\n\
	1 - filesystem IO errors\n\
	2 - command line arguments are invalid\n\
	3 - (unused on Windows)\n\
	4 - the user-provided regular expression did not match any games\n\
	5 - the user-provided regular expression did not compile, i.e. it was invalid\n\
	6 - the cache JSON could not be (de)serialized\n\
	7 - at least one VDF or ACF file could not be parsed\n\
	8 - either read permissions in at least one library directory were denied, or file paths are not valid Unicode";

#[cfg(not(target_family = "windows"))]
pub static LONG_ABOUT: &str =
	"Launches Steam games by name. Assumes the default steamapps directory, but a config file can override this; it \
	 need only contain a path to a steamapps directory. The config file is `~/.config/vapor/vapor.conf`. If a cache \
	 exists, search will use the cache instead of the steamapps directory; see -c\n\
	 \n\
	 Exit code 0 signals successful operation. Codes 1 through 8 signal the following errors:\n\
	 1 - filesystem IO errors\n\
	 2 - command line arguments are invalid\n\
	 3 - Vapor attempted to read the $HOME environment variable, but could not\n\
	 4 - the user-provided regular expression did not match any games\n\
	 5 - the user-provided regular expression did not compile, i.e. it was invalid\n\
	 6 - the cache JSON could not be (de)serialized\n\
	 7 - at least one VDF or ACF file could not be parsed\n\
	 8 - either read permissions in at least one library directory were denied, or file paths are not valid Unicode";

#[cfg(target_family = "windows")]
pub static CACHE_LONG_HELP: &str =
	"Create a JSON cache of the current games library to vastly accelerate search. If a cache already exists, it will \
	 be deleted THEN remade.\n\
	 Optionally, provide a path to a file containing a JSON array of games. An example file may read:\n\
	 [{\"foo\": 123}, {\"bar\": 456}] which contains foo with appid 123 and bar with appid 456. If no such path is \
	 provided, the default will be used: `C:\\Program Data\\Vapor\\cache.json`.\n\
	 A cache file can be specified as PATH. If the cache file does not exist when using -s or -l, the program will \
	 read from steamapps. Using this option with -s or -l without specifying a cache file is the \
	 same as not providing this option at all";

#[cfg(not(target_family = "windows"))]
pub static CACHE_LONG_HELP: &str =
	"Create a JSON cache of the current games library to vastly accelerate search. If a cache already exists, it will \
	 be deleted THEN remade.\n\
	 Optionally, provide a path to a file containing a JSON array of games. An example file may read:\n\
	 [{\"foo\": 123}, {\"bar\": 456}] which contains foo with appid 123 and bar with appid 456. If no such path is \
	 provided, the default will be used: `~/.local/share/vapor/cache.json`.\n\
	 A cache file can be specified as PATH. If the cache file does not exist when using -s or -l, the program will \
	 read from steamapps. Using this option with -s or -l without specifying a cache file is the \
	 same as not providing this option at all";
