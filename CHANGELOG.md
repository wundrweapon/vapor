## Vapor CLI v3.1.0, Vapor v2.0.0

- help output now varies by OS
- bugfix: Vapor can now read multiple libraries
- exit codes were changed to reflect internal changes
- config file removed, prompting a major version bump on the library
  + one could argue the exit codes do the same for the CLI, but I don't care
- basically reworked the entire library and handled errors correctly this time
  + I think I accidentally made the cache creation faster too?

## Vapor CLI v3.0.1

- fix cache generation

## Vapor v1.0.0, Vapor CLI v3.0.0

- new changelog format
- moved non-argparse logic to a library
  + the `vapor` package name now belongs to the library
  + likewise, generated library file is `libvapor.rlib`
- CLI now under `vapor-cli`
  + generated binary is still called `vapor`
- cleaner code in several places, most notably games.rs
- `Game` struct no longer has public fields
  + new trait implementations allow access to them by casting to 2-tuple
  + other trait implementations make `Game` more flexible, give 'em a look

## 2.0.1

- no user-facing changes
- slight code adjustments to satisfy myself

## 2.0.0

- use cache whenever possible
- specify dependency versions
- improve argument parsing slightly
- save reallocations in regex string formation
- dedicate return code 4 for when no matching game is found in launch mode

## 1.3.0

- add caching function
  + save games list to a JSON file
  + searching this cache is much faster (~8x system, ~53x user)
  + @CraftSpider wrote the deserializer, the absolute Chad
  + exit code 3 dedicated to cache-related errors
- attempt Windows support
- prevent `i` flag without either `s` or `l`
- slightly reduce CLI parsing jank
  + I'm still not too proud of it but it used to be worse
- improve code comments

## 1.2.4

- accelerate game search

## 1.2.3

- improve code comments
- handle invalid regex in search mode
  + this used to just panic
  + exit code 1 is now dedicated to this error
- reduce memory useage when searching for games
- accelerate game search

## 1.2.2

- reduce appmanifest parsing jank (@toyli)

## 1.2.1

- correct help text
- correct version
- correct macOS steamapps location

## 1.2.0

- rewrite in Rust
- parallelize file IO
- remove `N` and `S` options
  + add `i` flag to compensate
- attempt macOS support
  + a tiny bit of Windows support code is also there, but does nothing. yet.

## 1.1.0

- remove Lua requirement
  + all Lua code rewritten in C

## 1.0.0

- initial release
