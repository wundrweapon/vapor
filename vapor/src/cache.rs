use std::io::BufWriter;
use std::io::ErrorKind::{AlreadyExists, NotFound};
use std::path::PathBuf;
use std::fs;
use std::fs::File;

use lazy_regex::Regex;
use serde_json::{to_writer as write_json, from_str as read_json_str};

use crate::error::{Result, VaporError};
use crate::games::*;

/// Create and write a JSON cache file containing games' names and appids.
///
/// Any errors hailing from `std::fs` or `serde_json` will be passed back to the caller
pub fn build_cache(cache_path: &PathBuf) -> Result<()> {

	//List every game in steamapps
	//SAFETY: empty regex is not malformed
	let list = find_games("", false, &PathBuf::from("")).unwrap();

	//Make sure the `vapor` directory exists, or `File::create` will fail
	//In theory, I can ignore the `Some` check because unless the user foolishly provided root, there is a parent
	//TODO: clean this up; I suspect it can be better but have no proof
	let path_parent = cache_path.parent();
	if let Some(parent) = path_parent {
		match fs::create_dir(parent) {
			Ok(_) => {}
			Err(e) => match e.kind() {
				AlreadyExists | NotFound => {} //Parent exists or is empty string: not a problem
				_ => return Err(VaporError::IoError(e))
			}
		}
	}

	//Create or replace the cache file
	let cache = File::create(cache_path)?;
	write_json(BufWriter::new(cache), &list)?;

	Ok(())
}

/// Returns a list of games whose names match the provided regular expression. List is read from cache
pub fn read_cache(cache_path: &PathBuf, name_regex: &Regex) -> Result<Vec<Game>> {

	//The path is known to exist, but other IO errors could occur
	let json = fs::read_to_string(cache_path)?;

	//Malformed JSON will error, so users best leave it alone
	//TODO: parallelize
	let mut list: Vec<Game> = read_json_str::<Vec<Game>>(&json)?
		.into_iter()
		.filter(|g| name_regex.is_match(g.name())) //Drop non-matching names
		.collect();

	//Sort the list and return it
	list.sort_by(|a, b| a.name().cmp(b.name()));

	Ok(list)
}
