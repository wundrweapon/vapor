use std::{io, error::Error, fmt::{Display, Formatter, Result as FmtResult}};

#[cfg(not(target_family = "windows"))] //environment variables not used on Windows
use std::env;

pub type Result<T> = core::result::Result<T, VaporError>;

#[derive(Debug)]
pub enum VaporError {

	/// Environment variable $HOME not set
	#[cfg(not(target_family = "windows"))] //this is a waste of memory on a target that won't access it
	HomeNotLoaded(env::VarError),

	/// Steam's libraryfolders.vdf is not where it should be
	MissingLibraryFolders,

	/// VDF deserialization error. That type is so big it scares Clippy, so we put that on the heap
	VdfParseFailure(Box<keyvalues_serde::Error>),

	/// Glob failed because of invalid Unicode
	GlobFailure,

	/// User-provided regex is invalid
	InvalidRegex(regex::Error),

	/// Serde failed to (de)serialize the cache
	SerdeFailure(serde_json::Error),

	/// Any other IO-related error
	IoError(io::Error)
}
impl Display for VaporError {
	fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {

		//TODO: wasted memory allocations. `&format!(...)` doesn't live long enough, but would avoid n-1 allocs
		let error = match self {
			#[cfg(not(target_family = "windows"))] //this variant does not exist on windows
			Self::HomeNotLoaded(e) => format!("HOME environment variable could not be read: {e}"),

			Self::MissingLibraryFolders => "libraryfolders.vdf was not found".to_owned(),
			Self::VdfParseFailure(e) =>
				format!("libraryfolders.vdf was formatted unexpectedly: {e}"),
			Self::GlobFailure => "globbing for appmanifest files failed. Paths probably have invalid names".to_owned(),
			Self::InvalidRegex(e) => format!("Invalid regular expression: {e}"),
			Self::SerdeFailure(e) => format!("JSON cache (de)serialization error: {e}"),
			Self::IoError(e) => format!("IO error: {e}")
		};

		write!(f, "{error}")
	}
}
impl Error for VaporError {}

/// Allow `?` operator for encapsulated errors
macro_rules! impl_from {
	($t:ty as $variant:expr) => {
		impl From<$t> for VaporError {
			fn from(value: $t) -> Self {
				$variant(value)
			}
		}
	}
}

#[cfg(not(target_family = "windows"))] //this variant does not exist on windows
impl_from!(env::VarError as VaporError::HomeNotLoaded);

impl_from!(Box<keyvalues_serde::Error> as VaporError::VdfParseFailure);
impl_from!(regex::Error as VaporError::InvalidRegex);
impl_from!(serde_json::Error as VaporError::SerdeFailure);
impl_from!(io::Error as VaporError::IoError);

//Macro can't save our skin here because of the box.
//TODO: learn black magic macro shenanigans to handle this edge case in the macro
impl From<keyvalues_serde::Error> for VaporError {
	fn from(value: keyvalues_serde::Error) -> Self {
		Self::VdfParseFailure(Box::new(value))
	}
}
