use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

#[cfg(not(target_family = "windows"))] //environment variables not used on Windows
use std::env;

use serde::Deserialize;

use crate::error::{Result, VaporError};

/// Representation of the named and numbered libraries listed in libraryfolders.vdf
#[derive(Deserialize)]
struct Library {

	/// Path of the library, converted from a string to an owned path
	path: PathBuf
}

/// Value of the grander libraryfolders.vdf data.
/// Despite being a list by all accounts, this must be a map
#[derive(Deserialize)]
struct LibraryList(HashMap<usize, Library>);
impl LibraryList {

	/// Create a Steam library directory list from a VDF file. VDF parsing errors are boxed to account for their size
	fn from_file(file: File) -> Result<Self> {
		Ok(keyvalues_serde::from_reader::<_, Self>(BufReader::new(file))?)
	}
}

/// Returns a path to the VDF file holding all the libraries' paths. On Windows, this is
/// C:/Program Files (x86)/Steam/steamapps
#[cfg(target_family = "windows")]
fn get_libraryfolders() -> Result<PathBuf> {
	let path = PathBuf::from(r"C:\Program Files (x86)\Steam\steamapps\libraryfolders.vdf".to_owned());

	if path.exists() {
		Ok(path)
	} else {
		Err(VaporError::MissingLibraryFolders)
	}
}

/// Returns a path to the VDF file holding all the libraries' paths. Different on macOS and other Unix
#[cfg(not(target_family = "windows"))]
fn get_libraryfolders() -> Result<PathBuf> {
	let home = env::var("HOME")?;

	let path_string = home + match std::env::consts::OS {
		"macos" => "/Library/Application Support/Steam/steamapps/libraryfolders.vdf",
		_ => "/.local/share/Steam/steamapps/libraryfolders.vdf"
	};
	let path = PathBuf::from(path_string);

	if path.exists() {
		Ok(path)
	} else {
		Err(VaporError::MissingLibraryFolders)
	}
}

/// Returns a path to the cache file, C:/Program Data/Vapor/cache.json
#[cfg(target_family = "windows")]
#[inline(always)]
pub fn get_cache() -> Result<PathBuf> {
	Ok(PathBuf::from(r"C:\Program Data\Vapor\cache.json".to_owned()))
}

/// Returns a path to the cache file, /etc/vapor/cache.json
#[cfg(not(target_family = "windows"))]
pub fn get_cache() -> Result<PathBuf> {
	let home = env::var("HOME")?;
	Ok(PathBuf::from(home + "/.local/share/vapor/cache.json"))
}

/// Extract a list of steamapps library directories from the base install
pub fn find_libraries() -> Result<Vec<PathBuf>> {
	let libfolders = File::open(get_libraryfolders()?)?;

	let list = LibraryList::from_file(libfolders)?
		.0
		.into_iter() //this could be parallel with rayon; overhead likely exceeds performance
		.map(|entry| entry.1.path)
		.map(|path| path.join("steamapps"))
		.collect();

	Ok(list)
}
