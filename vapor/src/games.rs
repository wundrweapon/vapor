use std::fmt::{Formatter, Result as FormatResult};
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

use glob::{glob, GlobError};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use serde::{Serialize, Deserialize};
use serde::ser::SerializeMap;
use serde::de::Visitor as DeVisitor;
use lazy_regex::Regex;

use crate::cache;
use crate::dirs;
use crate::error::{Result as VaporResult, VaporError};

/// Represent a Steam game simply
#[derive(Clone)]
pub struct Game {
	appid: u32,
	name: String
}

impl Game {

	/// Create a new Game from an integer and owned string
	pub fn new(appid: u32, name: String) -> Self {
		Self {
			appid,
			name
		}
	}

	/// Create a new Game from an integer and unowned string
	pub fn new_str(appid: u32, name: &str) -> Self {
		Self {
			appid,
			name: name.to_owned()
		}
	}

	/// Copy the game's appid
	pub fn appid(&self) -> u32 {
		self.appid
	}

	/// Grab a reference to the game's name
	pub fn name(&self) -> &str {
		&self.name
	}
}

//Games can be compared in O(1) time, as Steam appids are unique.
//This assumes the strings match, which is fair because users cannot manipulate them
impl PartialEq<Game> for Game {
	fn eq(&self, rhs: &Self) -> bool {
		self.appid == rhs.appid
	}
}
impl Eq for Game {}

//Convert a Game to a pair of integer and owned string.
//Implicitly enables the reverse operation via tuple::into
impl From<Game> for (u32, String) {
	fn from(val: Game) -> Self {
		(val.appid, val.name)
	}
}

//Convert reference to a Game to a pair of integer and unowned string.
//Implicitly enables the reverse operation via tuple::into
impl<'a> From<&'a Game> for (u32, &'a str) {
	fn from(val: &'a Game) -> Self {
		(val.appid, &val.name)
	}
}

//Convert a struct like `Game {appid: 123, name: "foo"}` into JSON like `{"foo": 123}`
impl Serialize for Game {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where
			S: serde::Serializer {

		//Serialize like a HashMap
		let mut map = serializer.serialize_map(Some(1))?;
		map.serialize_entry(&self.name, &self.appid)?;
		map.end()
	}
}

//Convert JSON like `{"foo": 123}` into a struct like `Game {appid: 123, name: "foo"}`
//CraftSpider wrote all of this. I can't begin to explain how it works
impl<'de> Deserialize<'de> for Game {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
		where
			D: serde::Deserializer<'de> {

		struct Visitor;
		impl<'de> DeVisitor<'de> for Visitor {
			type Value = (String, u32);

			fn expecting(&self, formatter: &mut Formatter<'_>) -> FormatResult {
				write!(formatter, "Game instance")
			}

			fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
				where
					A: serde::de::MapAccess<'de>, {
				Ok(map.next_entry()?.unwrap())
			}
		}

		let (name, appid) = deserializer.deserialize_map(Visitor)?;

		Ok(Game::new(appid, name))
	}
}

/// Intermediate representational stage between VDF file and Game struct
//TODO: merge this struct and Game. May be difficult with how Deserialize was implemented on the other...
#[derive(Deserialize)]
struct VdfGame {
	appid: u32,
	name: String
}
impl VdfGame {

	/// Read a Steam game's appid and name from an appmanifest ACF file
	fn from_file(file: &File) -> VaporResult<Self> {
		Ok(keyvalues_serde::from_reader::<_, Self>(BufReader::new(file))?)
	}

	/// Read a Steam game's appid and name from a path to an appmanifest ACF file
	fn from_path(path: &PathBuf) -> VaporResult<Self> {
		let file = File::open(path)?;
		Self::from_file(&file)
	}
}

/// Helper function for `parse_appmanifests`. Used for graceful error handling when iterating over glob results
fn handle_globbed_path(find: Result<PathBuf, GlobError>) -> Option<PathBuf> {
	match find {
		Ok(path) => Some(path),
		Err(e) => {
			let filename = e.path()
				.to_str()
				.unwrap_or("[filename is not valid Unicode]");

			eprintln!("Could not read `{filename}`: permission denied");
			None
		}
	}
}

/// Helper function for `parse_appmanifests`. Used for graceful error handling when parsing appmanifest VDF files
fn try_parse_acf(path: &PathBuf) -> Option<VdfGame> {
	match VdfGame::from_path(path) {
		Ok(x) => Some(x),
		Err(e) => {
			eprintln!("VDFparsing error: {e}");
			None
		}
	}
}

/// Parse a directory full of appmanifest ACF files to extract games with name matching the provided regex. `library`
/// must be a steamapps directory. The final list is NOT sorted before returning
fn parse_appmanifests(library: &mut PathBuf, name: &Regex) -> VaporResult<Vec<Game>> {
	library.push("appmanifest_*.acf");
	let pattern = match library.to_str() {
		Some(s) => s,
		None => return Err(VaporError::GlobFailure)
	};

	//Glob for all ACF files in a library, and roll them into a list primed for parallel IO
	let files = glob(pattern)
		.unwrap() //the pattern is definitely valid
		.filter_map(handle_globbed_path)
		.collect::<Vec<_>>();

	//Read the ACF files in parallel and filter out non-matching titles
	let out = files.par_iter()
		.filter_map(try_parse_acf) //TODO: silent failure is less than ideal
		.filter_map(|vdf_game|
			if name.is_match(&vdf_game.name) {
				Some(Game::new(vdf_game.appid, vdf_game.name))
			} else {
				None
			}
		)
		.collect::<Vec<_>>();

	Ok(out)
}

/// Use the given regular expression to find Steam games. Returns a list of games if the provided regex is legit, or a
/// smelly user-caused error if it isn't. Other errors returning from this function are propagated up from elsewhere
pub fn find_games(name: &str, case_insensitive: bool, cache: &PathBuf)
	-> Result<Vec<Game>, VaporError>
{

	//Make the regex string. Pre-allocate the maximum needed space
	let mut re_str = String::with_capacity(4 + name.len());
	if case_insensitive {re_str.push_str("(?i)");}
	re_str.push_str(name);

	//Convert the string to a true regex and start the search
	let regex = Regex::new(&re_str)?;

	//If there is a cache, read from it
	if cache.exists() {
		return cache::read_cache(cache, &regex);
	}

	//Otherwise, read from steamapps directories

	//TODO: each library is parallelized, but we could in theory do *all* ACF files simultaneously
	let mut list = dirs::find_libraries()?;
	let mut out = Vec::new();
	for path in list.iter_mut() {
		out.append(&mut parse_appmanifests(path, &regex)?);
	}

	out.sort_by(|a, b| a.name().cmp(b.name()));

	Ok(out)
}
