use std::process::{Command, Child};

pub mod cache;
pub mod dirs;
pub mod games;
pub mod error;

use error::Result;
use games::Game;

/// Launch a given appid.
///
/// Notes:
///
/// - Steam silently fails on invalid appid; no error is generated
/// - the format string and Steam URI are known to be valid
/// - if the command returns an error, well, I don't know what happened. Is Steam installed?
#[inline(always)]
pub fn launch_appid(appid: u32) -> Result<Child> {
	Ok(Command::new("steam")
		.arg(format!("steam://rungameid/{appid}"))
		.spawn()?)
}

/// Launch a given Game. See launch_appid for additional notes.
#[inline(always)]
pub fn launch_game(game: &Game) -> Result<Child> {
	launch_appid(game.appid())
}
