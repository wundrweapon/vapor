# Vapor

I am so sick and tired of the Steam client grinding to a halt during downloads. For that reason alone, I wrote Vapor to
launch games without having to use the client.

The Vapor CLI was separated from the libvapor library in May of 2023 when [a friend of mine](http://toyli.dev) casually
mentioned making their own Steam launcher GUI. Since a lot of the hard work was already done in Vapor, I offered to make
it into a library.

## Usage

Simply run `vapor -h` to get an idea of how to use this thing; it's not exactly rocket science. Even just `vapor` will
print usage info. Alternatively, `vapor --help` will give more detailed info. Usually, I do `vapor -in "game title"`.

## Building and dependencies

Cargo handles dependencies automatically, as all are available on [crates.io](https://crates.io).

If you're only interested in the library, `cargo b` will generate `target/debug/libvapor.rlib`.  
If you're interested in the CLI, `cargo b -p vapor-cli` will generate `target/debug/vapor`.  
If you're interested in both, `cargo b --workspace` will generate both.

In any case, `-r` will optimize the file (smaller binary and faster execution) and place it in `target/release/`.

For those with access to a POSIX-compliant shell (or superset like bash or zsh), executing `build.sh` will generate a
*highly* optimized binary which removes some panicking code. This will build for an x86_64 GNU/Linux OS, but can easily
be edited to build for other systems. Alone, this will compile only `vapor`, but options provided to `build.sh` are
passed directly to cargo. Thus, the following do as one would expect:

- `./build.sh`, identical to `./build.sh -p vapor`
- `./build.sh -p vapor-cli`
- `./build.sh --workspace`

Because this uses panic-immediate-abort, the nightly channel for your target must be installed. For the default target,
you can achieve that with `rustup install nightly`.

## OSes other than Linux

Right now, only GNU/Linux is officially supported. Some code exists to hopefully work on macOS but that is untested.
Windows support "exists" but is early and known not to work. If you've confirmed Vapor works on something else (BSD,
non-GNU Linux, Haiku, etc.) then please let me know. If you want to contribute Windows support *please* make a merge
request, as I will probably never do that myself.
